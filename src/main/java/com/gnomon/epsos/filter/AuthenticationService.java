package com.gnomon.epsos.filter;

import com.gnomon.epsos.rest.EpsosRestService;
import com.liferay.portal.model.CompanyConstants;
import com.liferay.portal.service.UserLocalServiceUtil;
import java.io.IOException;
import java.util.StringTokenizer;
import org.apache.commons.codec.binary.Base64;

public class AuthenticationService {

    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger("AuthenticationService");

    public boolean authenticate(String authCredentials) {
        logger.info("try to authencticate with: " + authCredentials);
        if (null == authCredentials) {
            return false;
        }
        // header value format will be "Basic encodedstring" for Basic
        // authentication. Example "Basic YWRtaW46YWRtaW4="
        final String encodedUserPassword = authCredentials.replaceFirst("Basic"
                + " ", "");
        String usernameAndPassword = null;
        try {
            byte[] decodedBytes = Base64.decodeBase64(encodedUserPassword);
            usernameAndPassword = new String(decodedBytes, "UTF-8");
            logger.info("Decoded password: " + usernameAndPassword);
        } catch (IOException e) {
            e.printStackTrace();
        }
        final StringTokenizer tokenizer = new StringTokenizer(
                usernameAndPassword, ":");
        boolean authenticationStatus = false;
        try {
            final String username = tokenizer.nextToken();
            final String password = tokenizer.nextToken();
            logger.info("username: " + username + " password: " + password);
            // we have fixed the userid and password as admin
            // call some UserService/LDAP here
            long auth = UserLocalServiceUtil.authenticateForBasic(EpsosRestService.companyId,
                    CompanyConstants.AUTH_TYPE_SN,
                    username, password);
            logger.info("Authentication status for username: " + username + " is " + auth);
            authenticationStatus = auth > 0;
//            authenticationStatus = "admin".equals(username)
//                    && "admin".equals(password);
        } catch (Exception e) {
            logger.error("Error decoding username password");
        }
        return authenticationStatus;
    }
}
