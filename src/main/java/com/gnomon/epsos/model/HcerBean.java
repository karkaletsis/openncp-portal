package com.gnomon.epsos.model;

import com.gnomon.LiferayUtils;
import com.gnomon.epsos.hcer.HCERDocs;
import com.gnomon.epsos.hcer.HcerPersistentManager;
import com.liferay.portal.model.User;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.orm.PersistentException;
import org.orm.PersistentSession;

@ManagedBean
@ViewScoped
public class HcerBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private List<HCERDocs> docs;
    private static final Logger log = Logger.getLogger("HcerBean");

    public HcerBean() throws PersistentException {
        log.info("quering hcer docs");
        User user = LiferayUtils.getPortalUser();
        LiferayUtils.storeToSession("user", user);
        PersistentSession s = HcerPersistentManager.instance().getSession();
        try {
            docs = HCERDocs.queryHCERDocs(null, "creationDate desc");
            log.info(docs.size());
        } catch (PersistentException ex) {
            log.error(ExceptionUtils.getStackTrace(ex));
        } finally {
            s.close();
        }
    }

    public List<HCERDocs> getDocs() {
        return docs;
    }

    public void setDocs(List<HCERDocs> docs) {
        this.docs = docs;
    }

}
